import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'assignment2-frontend';

  ngOnInit(): void{
    //localStorage.setItem('isEnglish', 'false');
  }

  selectLanguage(lang: string){
    lang === 'en' ? localStorage.setItem('isEnglish', 'true') : localStorage.setItem('isEnglish', 'false');
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }
}



