import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { ServicesComponent } from './services/services.component';
import { TeamComponent } from './team/team.component';


const routes: Routes = [
  { path:'', redirectTo:'/home', pathMatch:'full'} ,
  { path: 'home', component: HomePageComponent },
  { path: 'noutati', component: NewsPageComponent },
  { path: 'despre-noi', component: AboutUsComponent },
  { path: 'medici', component: TeamComponent },
  { path: 'servicii-tarife', component: ServicesComponent },
  { path: 'contact', component: ContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
