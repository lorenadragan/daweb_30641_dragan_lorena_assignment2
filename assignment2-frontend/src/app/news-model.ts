export class NewsModel {
  title: string;
  paragraph1: string;
  paragraph2: string;
  paragraph3: string;
}
