import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  showSubmenu1: boolean;
  showSubmenu2: boolean;
  showSubmenu3: boolean;
  showSubmenu4: boolean;
  showSubmenu5: boolean;
  showSubmenu6: boolean;
  showSubmenu7: boolean;
  showSubmenu8: boolean;
  showSubmenu9: boolean;
  showSubmenu10: boolean;
  showSubmenu11: boolean;
  showSubmenu12: boolean;
  constructor() { }

  ngOnInit(): void {
    this.showSubmenu1 = false;
    this.showSubmenu2 = false;
    this.showSubmenu3 = false;
    this.showSubmenu4 = false;
    this.showSubmenu5 = false;
    this.showSubmenu6 = false;
    this.showSubmenu7 = false;
    this.showSubmenu8 = false;
    this.showSubmenu9 = false;
    this.showSubmenu10 = false;
    this.showSubmenu11 = false;
    this.showSubmenu12 = false;
  }
  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

}
