import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as xml2js from 'xml2js';
import { NewsModel } from '../news-model';
@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.css']
})
export class NewsPageComponent implements OnInit {

  xmlContent: string;
  englishNews: Array<NewsModel>;
  romanianNews: Array<NewsModel>;

  constructor(private _http: HttpClient) {
   }

  ngOnInit(): void {
    this.englishNews = new Array<NewsModel>();
    this.romanianNews = new Array<NewsModel>();

    this.getXmlRomanianNews();
    this.getXmlEnglishNews();
  }

  async getXmlRomanianNews(){
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'text/xml');
    await this._http.get('assets/news-ro.xml', {headers, responseType: 'text'})
      .subscribe(output => {
        xml2js.parseString(output, (err, result) => {
          for(let index = 0; index < (result["NewsList"]["News"]).length; index++){
            let news: NewsModel = new NewsModel();
            news.title = result["NewsList"]["News"][index]["Title"];

            news.paragraph1 = result["NewsList"]["News"][index]["Content"][0]["Paragraph1"];
            news.paragraph2 = result["NewsList"]["News"][index]["Content"][0]["Paragraph2"];
            news.paragraph3 = result["NewsList"]["News"][index]["Content"][0]["Paragraph3"];

            this.romanianNews.push(news);
          }
        })
      }, err => console.log(err));
  }

  async getXmlEnglishNews(){
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'text/xml');
    await this._http.get('assets/news-en.xml', {headers, responseType: 'text'})
      .subscribe(output => {
        xml2js.parseString(output, (err, result) => {
          for(let index = 0; index < (result["NewsList"]["News"]).length; index++){
            let news: NewsModel = new NewsModel();
            news.title = result["NewsList"]["News"][index]["Title"];

            news.paragraph1 = result["NewsList"]["News"][index]["Content"][0]["Paragraph1"];
            news.paragraph2 = result["NewsList"]["News"][index]["Content"][0]["Paragraph2"];
            news.paragraph3 = result["NewsList"]["News"][index]["Content"][0]["Paragraph3"];

            this.englishNews.push(news);
          }
        })
      }, err => console.log(err));
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }
}
